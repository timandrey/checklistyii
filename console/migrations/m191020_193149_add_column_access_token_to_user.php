<?php

use yii\db\Migration;

/**
 * Class m191020_193149_add_column_access_token_to_user
 */
class m191020_193149_add_column_access_token_to_user extends Migration
{
    public function up()
    {
        $ret = $this->DB->createCommand("SELECT * FROM information_schema. columns WHERE table_schema = DATABASE () and table_name ='user'and column_name ='access_token'")->queryOne();
        if (empty($ret)) {
            $this->addColumn('user','access_token', $this->string(255)->defaultValue(NULL)->comment ('token'));
        }
    }

    public function down()
    {
        $this->dropColumn('user', 'access_token');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191020_193149_add_column_access_token_to_user cannot be reverted.\n";

        return false;
    }
    */
}
