<?php

use yii\db\Migration;

/**
 * Class m191018_093944_add_banned_columns_table_users
 */
class m191018_093944_add_banned_columns_table_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'banned', 'boolean');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191018_093944_add_banned_columns_table_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191018_093944_add_banned_columns_table_users cannot be reverted.\n";

        return false;
    }
    */
}
