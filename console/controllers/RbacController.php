<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\rbac\AuthorRule;
class RbacController extends Controller {

    public function actionInit() {
        $auth = Yii::$app->authManager;

        $user = $auth->getRole('user');
        $admin = $auth->getRole('admin');
        $manager = $auth->getRole('manager');

       // // Создаем наше правило, которое позволит проверить автора новости
      //  $authorRule = new AuthorRule;

        // Запишем его в БД
       // $auth->add($authorRule);



        // Создадим еще новое разрешение «Редактирование собственной новости» и ассоциируем его с правилом AuthorRule
      //  $updateOwnNews = $auth->createPermission('updateOwnNews');
       // $updateOwnNews->description = 'edit checklists';

        $checkadmin = $auth->createPermission('checkAdminPanel');
        $checkadmin->description = 'check adminpanel';
        $auth->add($checkadmin);
      //  $auth->addChild($admin, $checkadmin);
      //  $auth->addChild($manager, $checkadmin);
        // Указываем правило AuthorRule для разрешения updateOwnNews.
       // $updateOwnNews->ruleName = $authorRule->name;

        // Запишем все разрешения в БД


        // Теперь добавим наследования. Для роли editor мы добавим разрешение updateOwnNews (редактировать собственную новость),
        // а для админа добавим собственные разрешения viewAdminPage и updateNews (может смотреть админку и редактировать любую новость)

        // Роли «Редактор новостей» присваиваем разрешение «Редактирование собственной новости»
       // $auth->addChild($user, $updateOwnNews);
       // $auth->addChild($admin, $updateOwnNews);
     //   $auth->addChild($manager, $updateOwnNews);

    }
}