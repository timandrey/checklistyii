<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$form = ActiveForm::begin(['action' => 'add-role'], ['method' => 'post']);
echo $form->field($add_role_form, 'name');
echo $form->field($add_role_form, 'description');
echo '<div class="form-group">';
echo Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']);
echo '</div>';
ActiveForm::end();