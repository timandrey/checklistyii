<?php
/* @var $this yii\web\View */

use yii\bootstrap\NavBar;
use yii\web\View;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = 'My Yii Application';
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <thead>
                    <tr>
                        <th>username</th>
                        <th>Email</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $nameCsrf = Yii::$app->request->csrfParam;
                    $valCsrf = Yii::$app->request->csrfToken;
                    foreach ($users as $user) {
                        if ($user_roles = Yii::$app->authManager->getRolesByUser($user->id)) {
                            $user_role = array_shift($user_roles)->name;
                            if ($user_role == 'admin') {
                                continue;
                            }
                            if ($user_role == 'manager' && Yii::$app->user->can('manager')) {
                                continue;
                            }
                        }
                            echo "<tr>";
                            echo " <td>$user->username</td>";
                            echo " <td>$user->email</td>";
                            if (Yii::$app->user->can('admin')) {
                                echo "<td>
                                        <form action='delete' method='post'>
                                            <input type='hidden' name='$nameCsrf' value='$valCsrf'>
                                            <input type='hidden' name='user_id' value='$user->id' >
                                            <button class='btn btn-danger' type='submit'>Delete</button>
                                        </form>
                                      </td>";
                                echo "<td>
                                        <form action='give-role' method='post'>
                                            <input type='hidden' name='$nameCsrf' value='$valCsrf'>
                                            <input type='hidden' name='user_id' value='$user->id' >
                                            <label for=\"exampleFormControlSelect1\">Example select</label>
                                            <select class='form-control' name='role'>
                                            ";
                                            foreach ($roles as $role) {
                                                echo "<option>$role->name</option>";
                                            }
                                             echo "</select><button class='btn btn-primary' type='submit'>Give Role</button></form></td>";
                                echo "<td>
                                        <form action='change-max-list' method='post'>
                                            <input type='hidden' name='$nameCsrf' value='$valCsrf'>
                                            <input type='hidden' name='user_id' value='$user->id'>
                                            <input type='text' name='max_lists' placeholder='max lists'>
                                            <button class='btn btn-warning' type='submit'>Change</button>
                                        </form>
                                      </td>";
                            }
                                 if ($user->banned == true) {
                                    echo "<td>
                                            <form action='un-ban' method='post'>
                                                <input type='hidden' name='$nameCsrf' value='$valCsrf'>
                                                <input type='hidden' name='user_id' value='$user->id' >
                                                <button class='btn btn-warning' type='submit'>unban</button>
                                            </form>
                                          </td>";
                                } else {
                                    echo "<td>
                                            <form action='ban' method='post'>
                                                <input type='hidden' name='$nameCsrf' value='$valCsrf'>
                                                <input type='hidden' name='user_id' value='$user->id'>
                                                <button class='btn btn-warning' type='submit'>ban</button>
                                            </form>
                                         </td>";
                                }
                                echo "<td>";
                                    Modal::begin([
                                        'header' => '<h2>editing</h2>',
                                        'toggleButton' => [
                                            'label' => 'editing',
                                            'tag' => 'button',
                                            'class' => 'btn btn-success',
                                        ],
                                    ]);
                                    $form = ActiveForm::begin(['action' => 'edit'], ['method' => 'post']);
                                    echo $form->field($edit_user_form, 'username');
                                    echo $form->field($edit_user_form, 'email');
                                    echo $form->field($edit_user_form, 'user_id')->hiddenInput(['value' => $user->id])->label(false);
                                    echo '<div class="form-group">';
                                    echo Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']);
                                    echo '</div>';
                                    ActiveForm::end();
                                    Modal::end();;
                                    echo "</td>";
                                    echo "<td><a class='btn btn-secondary' href='/admin/checklists/$user->id'>checklists</a></td>";
                                echo "</tr>";
                        }
                    ?>
                    </tbody>
                </table>
                <a href="add-user">Добавить Пользователя</a>
                <?php use yii\widgets\LinkPager;
                echo LinkPager::widget([
                    'pagination' => $pages,
                ]);
                ?>
            </div>
        </div>
    </div>

