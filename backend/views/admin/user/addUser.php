<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$form = ActiveForm::begin(['action' => 'add-user'], ['method' => 'post']);
echo $form->field($add_user_form, 'username');
echo $form->field($add_user_form, 'email');
echo $form->field($add_user_form, 'password');
echo '<div class="form-group">';
echo Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']);
echo '</div>';
ActiveForm::end();