<?php


namespace backend\controllers\admin;

use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;
use yii\web\Controller;
use backend\models\User;
use backend\models\UserEditForm;
use backend\models\AddUserForm;
use common\models\SignupForm;
use yii\data\Pagination;

class UserController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['accept'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['index', 'add-user', 'ban', 'un-ban', 'edit'],
                    'allow' => true,
                    'roles' => ['manager', 'admin'],
                ],
                [
                    'actions' => ['delete', 'give-role', 'change-max-list'],
                    'allow' => true,
                    'roles' => ['admin'],
                ],
            ],
        ];


        return $behaviors;
    }
    public function actionIndex()
    {
        $query = User::find();
        $pages = new Pagination(['totalCount' => $query->count()]);

        $users = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        $edit_user_form = new UserEditForm();

        $roles = Yii::$app->authManager->getRoles();

        return $this->render('admin' , [
            'roles' => $roles,
            'users' => $users,
            'pages' => $pages,
            'edit_user_form' => $edit_user_form,
        ]);
    }

    public function actionDelete()
    {
        $user_id = Yii::$app->request->post('user_id');
        $user = User::findOne($user_id);
        $user->delete();
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionBan()
    {
        $user_id = Yii::$app->request->post('user_id');
        $user = User::findOne($user_id);
        $user->ban();
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionUnBan()
    {
        $user_id = Yii::$app->request->post('user_id');
        $user = User::findOne($user_id);
        $user->unBan();
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionGiveRole()
    {
        $role = (Yii::$app->request->post('role'));
        $user_id = Yii::$app->request->post('user_id');
        $userRole = Yii::$app->authManager->getRole($role);
        Yii::$app->authManager->revokeAll($user_id);
        Yii::$app->authManager->assign($userRole, $user_id);


        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }
    public function actionChangeMaxList()
    {
        $user_id = Yii::$app->request->post('user_id');
        $max_lists = Yii::$app->request->post('max_lists');
        $user = User::findOne($user_id);
        $user->changeMaxList($max_lists);
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionAddUser()
    {
        $add_user_form = new SignupForm();

        if ($add_user_form->load(Yii::$app->request->post()) && $add_user_form->validate()) {
            $add_user_form->signup();
            return $this->goBack();
        }

        return $this->render('addUser', compact('add_user_form'));
    }

    public function actionEdit()
    {
        $model_form = new UserEditForm();

        if ($model_form->load(Yii::$app->request->post()) && $model_form->validate()) {
            $user_id = $model_form->user_id;
            $username = $model_form->username;
            $email = $model_form->email;

            $user = User::findOne($user_id);

            if (!empty($username))
                $user->username = $username;
            if (!empty($email))
                $user->email = $email;

            $user->save();
        }
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }
}