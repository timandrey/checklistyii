<?php


namespace backend\controllers\admin;

use backend\models\Checklist;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use common\models\User;

class ChecklistController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['accept'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['show', 'index'],
                    'allow' => true,
                    'roles' => ['manager', 'admin'],
                ],
            ],
        ];
        return $behaviors;
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionShow($user_id)
    {
        $checklists = User::findOne($user_id)->checklists;
        return $this->render('index',['checklists' => $checklists]);
    }

}