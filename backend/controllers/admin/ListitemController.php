<?php


namespace backend\controllers\admin;

use backend\models\Checklist;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use backend\models\User;

class ListitemController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['accept'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['show', 'index'],
                    'allow' => true,
                    'roles' => ['manager', 'admin'],
                ],
            ],
        ];
        return $behaviors;
    }

    public function actionShow($list_id)
    {
        $listitems = Checklist::findOne($list_id)->listitems;
        return $this->render('index',['listitems' => $listitems]);
    }

    public function actionIndex()
    {
        Yii::$app->view->params['users'] = User::find()->all();;
        return $this->render('admin');
    }

}