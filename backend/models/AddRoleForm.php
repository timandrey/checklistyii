<?php


namespace backend\models;

use yii\base\Model;

class AddRoleForm extends Model
{
    public $name;
    public $description;

    public function rules() {
        return [
            [['name', 'description'], 'string', 'max' => 33],
        ];
    }
}