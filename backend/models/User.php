<?php
namespace backend\models;

use yii\db\ActiveRecord;

class User extends ActiveRecord
{
    public function getChecklists()
    {
        return $this->hasMany(Checklist::className(), ['user_id' => 'id']);
    }

    public function ban() {
        $this->banned = true;
        return $this->save();
    }

    public function unBan() {
        $this->banned = false;
        return $this->save();
    }

    public function checkBan() {
        return $this->banned;
    }

    public function changeMaxList($max_lists) {
        $this->max_lists = $max_lists;
        return $this->save();
    }
}