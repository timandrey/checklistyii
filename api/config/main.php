<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'api\controllers',
    'components' => [
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
                'text/xml' => 'yii\web\XmlParser',
            ]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'enableSession' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                'check' => 'user/check',
                'login' => 'auth/login',
                'signup' => 'auth/signup',
                'logout'=>'auth/logout',

//                '' => 'site/index',
//                'users/<user_id>/checklists/<list_id>' => 'checklist/check',
                ['class' => 'yii\rest\UrlRule', 'controller' => 'checklist'],
//                ['class' => 'yii\rest\UrlRule', 'controller' => 'user'],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'listitem'],
                'GET checklists/<list_id>/listitems' => 'listitem/index',
                'GET checklists/<list_id>/listitems/<item_id>' => 'listitem/view',
                'POST checklists/<list_id>/listitems' => 'listitem/create',
                'DELETE checklists/<list_id>/listitems/<item_id>' => 'listitem/delete',
                'PUT checklists/<list_id>/listitems/<item_id>' => 'listitem/update',
//                '<controller:\w+>/<id:\d+>' => '<controller>/view',
//                '<controller:\w+>/<id:\d+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
//                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
//                '<module:\w+>/<controller:\w+>/<id:\d+>' => '<module>/<controller>/view',
//                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
            ],
        ]
    ],
    'params' => $params,
];
