<?php


namespace api\controllers;

use common\models\User;
use Yii;
use yii\base\Controller;
use yii\filters\auth\HttpBearerAuth;
use api\models\auth\LoginForm;
use api\models\auth\SignupForm;
use yii\filters\VerbFilter;

class AuthController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'except' => ['login', 'signup'],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'signup' => ['POST'],
                'login' => ['POST'],
                'logout' => ['POST'],
            ],
        ];

        return $behaviors;
    }

    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->login()) {
            return Yii::$app->api->sendSuccessResponse( ['access_token' => $model->login()]);
        } else {
            return Yii::$app->api->sendFailedResponse($model->getFirstErrors());
        }
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post(), '') && $model->signup()) {
            return Yii::$app->api->sendSuccessResponse(['account create']);
        }
        else {
            return Yii::$app->api->sendFailedResponse($model->getFirstErrors());
        }
    }

    public function actionLogout()
    {
        $user = User::findOne(Yii::$app->user->id);

        if ($user->resetAccessToken()) {
            return Yii::$app->api->sendSuccessResponse(["Logged Out Successfully"]);
        } else {
            return Yii::$app->api->sendFailedResponse("Invalid Request");
        }
    }
}