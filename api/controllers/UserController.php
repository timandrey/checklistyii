<?php


namespace api\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;


class UserController extends ActiveController
{
    public $modelClass = 'common\models\User';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'signup' => ['POST'],
                'login' => ['POST'],
                'logout' => ['POST'],
                'check' => ['GET'],
            ],
        ];

        return $behaviors;
    }

    public function actionCheck()
    {
        return ['sfssdfsdf'];
    }

    public function actionIndex()
    {
        return ['sfssdfsdf'];
    }
}