<?php


namespace api\controllers;

use api\models\CreateChecklist;
use common\models\User;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;
use yii\rest\ActiveController;
use api\models\Checklist;


class ChecklistController extends ActiveController
{
    public $modelClass = 'api\models\Checklist';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'show' => ['GET, HEAD'],
                'create' => ['POST'],
            ],
        ];


        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete'],
              $actions['create'],
              $actions['index'],
              $actions['view'],
              $actions['update']);

        return $actions;
    }

    public function actionIndex() {
        $checklists = User::findOne(Yii::$app->user->id)->checklists;
        return Yii::$app->api->sendSuccessResponse([$checklists]);
    }

    public function actionView($id) {
        $checklist = Checklist::findOne($id);
        if (Yii::$app->user->can('updateOwnNews', ['checklist' => $checklist])) {
            return Yii::$app->api->sendSuccessResponse([$checklist]);
        }
        else {
            return Yii::$app->api->sendFailedResponse(['dont show']);
        }
    }

    public function actionCreate() {
        $create_list_model = new CreateChecklist();

        if ($create_list_model->load(Yii::$app->getRequest()->getBodyParams(), '') && $create_list_model->create()) {
            return Yii::$app->api->sendSuccessResponse(['create true']);
        }
        else {
            return Yii::$app->api->sendFailedResponse(['create false']);
        }
    }

    public function actionDelete($id) {
        $checklist = Checklist::findOne($id);

        if (Yii::$app->user->can('updateOwnNews', ['checklist' => $checklist])) {
            $checklist->delete();
            return Yii::$app->api->sendSuccessResponse(['delete true']);
        }
        else {
            return Yii::$app->api->sendFailedResponse(['dont delete']);
        }
    }

    public function actionUpdate($id) {
        $checklist = Checklist::findOne($id);

        if (Yii::$app->user->can('updateOwnNews', ['checklist' => $checklist])) {
            $title = Yii::$app->request->post('title');
            $checklist->title = $title;
            $checklist->save();
            return Yii::$app->api->sendSuccessResponse([$checklist]);
        }
        else {
            return Yii::$app->api->sendFailedResponse(['dont update']);
        }
    }

}