<?php


namespace api\models;

use yii\db\ActiveRecord;

class Checklist extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%checklist}}';
    }

    public function getListitems()
    {
        return $this->hasMany(Listitem::className(), ['checklist_id' => 'id']);
    }
}